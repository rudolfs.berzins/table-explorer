# Table Explorer Project

#### Purpose

Developed as part of a coding challenge.

Main tools used:
* Python
* [SQLParse](https://sqlparse.readthedocs.io/en/latest/intro/)
* [Poetry](https://python-poetry.org/)
* [Pytest](https://docs.pytest.org/en/6.2.x/)
* [Docker](https://www.docker.com/)


## DESCRIPTION

`Table Explorer Project` allows users to generate table hierarchies for either all config files in folder or a specific target table.


## INSTALLATION

To run the program please ***pull*** the GIT repository on your local machine using

```bash
git clone https://gitlab.com/rudolfs.berzins/table-explorer.git
```

It is possible to choose from 3 installation options: Docker and Local Isolated (using Python virtual environments) and Local

### Docker - Recommended

**Prerequisite** - Docker, Docker CLI and Docker Compose need to be present on the system. Follow instructions on their [website](https://docs.docker.com/get-docker/)

Execute the following command while in root directory of the project.

```bash
./run_in_docker.sh
```

This command will spin up a docker image that will contain `table_explorer` already installed. 
After that follow instructions described in Usage section. 
The shell script will also enter the docker container to allow usage of program in isolated environment. 
In addition, Docker will mount the current working directory as a Volume for the Docker image to be able to save `table_explorer` output.
To exit the container write `exit` or press CTRL(or CMD on Mac)+D.

Afterwards execute the following command to teardown the created environment in Docker.

```bash
./teardown_docker.sh
```
### Local Isolated Install - Alternative

**Prerequisite** - Have Python 3.8+ present on the system with `python-venv` package installed. Starting from Python 3+ `python-venv` should be preinstalled on most systems.

Execute the following command while in root directory of the project.
```bash
source run_locally.sh
```

This command will create a Python virtual environment and will install `table_explorer` using `poetry`.
After that follow instructions described in Usage section.
It will also activate the virtual environment creating an isloated environment where `table_explorer` can be used.
To exit this virtual environment, write `deactivate` in command line.

### Local Install - Not Recommended

**Prerequisite** - Have Python 3.8+ present on the system with `poetry` installed globally.

Execute the following command while in root directory of the project.
```bash
poetry install
```

This command will install `table_explorer` and it's dependencies in your system directly.
It might have unintended consequences on your local python environment and it is generaly not recommended. 


## USAGE

After installation `table_explorer` is ready for use. 

To see help write

```bash
table_explorer -h
```

**Help**

```bash
usage: table_explorer [-h] -c CONFIG_FOLDER [-a] [-t [TARGET_TABLES [TARGET_TABLES ...]]]
                      [--show_tables] [--save_as_markdown]
                      [--output_folder_path OUTPUT_FOLDER_PATH]

Given a folder containing JSON config files, generate table hierarchies for either all config
files in folder or a specific target table.

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG_FOLDER, --config_folder CONFIG_FOLDER
                        Path to folder containing JSON configuration files
  -a, --all             Display hierarchies for all configuration files in the given folder. This
                        option is overridden if '-t' option is present!
  -t [TARGET_TABLES [TARGET_TABLES ...]], --target_tables [TARGET_TABLES [TARGET_TABLES ...]]
                        Denotes which table (or tables) to target. The tables configuration file
                        MUST be present in the configuration folder (given by '-c' parameter)
  --show_tables         Display all available tables from the config folder
  --save_as_markdown    If set the output of the individual hierarchies will be saved into a
                        folder given by '--output_folder_path' argument. Default is current
                        working directory.
  --output_folder_path OUTPUT_FOLDER_PATH
                        If '--save_as_markdown' is present then stores the path to the folder
                        where the individual hierarchy output files need to be saved. Default is
                        current working directory.
```

***Example usage***

* Using a folder `data/` which contains JSON config files print a hierarchy on screen for table in config file `table.config.json`

```bash
table_explorer -c data/ -t table.config
```

* Using a folder `data/` which contains JSON config files print all possible table hierarchies contained in the folder

```bash
table_explorer -c data/ --all
```

* Using a folder `data/` which contains JSON config files print a hierarchy on screen for tables `table1.config` and `table2.config` given that their respective JSON files are present

```bash
table_explorer -c data/ -t table1.config table2.config
```

* Using a folder `data/` which contains JSON config files generate a hierarchy for `table.config` and save it as markdown in folder `hierarchy_output/`

```bash
table_explorer -c data/ -t table.config --save_as_markdown --output_folder_path hierarchy_output
```

## TESTING

To run all tests, after installation execute the following command in the root project directory

```bash
pytest tests
```

*Note* - it might be necessary to write `source table_explorer_venv/bin/activate` if using **Local Isolated Install** and it throws an error mentioning that pytest is not installed.

There are a number of levels for tests marked so it is possible to execute *unit*, *integration* and *validation* tests seperately from each other using the following commands

```bash
pytest tests -m unit
pytest tests -m integration
pytest tests -m validation
```

### Credits

Developed by [Rudolfs Berzins](https://www.linkedin.com/in/rudolfsberzinsbinf/)

All inquiries about code please send via [email](rudolfs.berzins.bio@gmail.com)


### Future improvements and TODO
* For this to be a full fledeged service it is probably necessary to be able to display the table configs in a full graph so there is no repetition between config hierarcy displays
* Need to find a way to create tests with fake `sqlparse.sql.Identifier` objects









