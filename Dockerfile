#-------------------------- DEPENDENCIES ------------------------#
FROM python:3.8-slim

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.1.11

RUN pip install poetry

WORKDIR /table_explorer
COPY poetry.lock pyproject.toml /table_explorer/

RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

COPY . /table_explorer



