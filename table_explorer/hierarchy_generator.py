import os

import sqlparse

from table_explorer.utils.exceptions import MissingTargetTableName
from table_explorer.utils.helpers import read_json_file, flatten


class HierarchyGenerator:
    def __init__(self, config_folder_path, all_indicator=False):
        self._config_folder_path = config_folder_path
        self._available_table_configs = self._extract_available_configs(
            self._config_folder_path
        )
        self._all_indicator = all_indicator
        self.base_hierarchy = {}

    def generate_hierarchies(self, target_tables=[None]):

        hierarchies_list = []

        for target_table in target_tables:
            if target_table and target_table in self._available_table_configs:
                hierarchies_list.append(
                    self._generate_single_target_table_hierarchy(target_table)
                )
            elif target_table is None and self._all_indicator:
                for target_t in self._available_table_configs:
                    hierarchies_list.append(
                        self._generate_single_target_table_hierarchy(target_t)
                    )
            else:
                raise MissingTargetTableName(
                    "Target Table Name is missing or incorrect. Please provide correct target table name or indicate that ALL hiererchies need to be generated with '--all' flag"
                )

        return hierarchies_list

    def _parse_single_config_dict(self, flat_config_dict):

        from_clauses = self._get_from_clauses(flat_config_dict)

        sources = (
            set()
        )  # Using sets because some clauses perform joins on same table twice

        for clause in from_clauses:
            clean_identifiers = self._parse_single_clause(clause)
            sources = sources.union(self._sanitize_identifiers(clean_identifiers))

        return sources

    def _generate_base_hierarchy(self, target_table):

        config_dict = read_json_file(
            os.path.join(
                os.path.abspath(self._config_folder_path), f"{target_table}.json"
            )
        )
        flat_config_dict = flatten(config_dict)
        sources = self._parse_single_config_dict(flat_config_dict)

        self.base_hierarchy[target_table] = sources

    def _generate_single_target_table_hierarchy(self, target_table=None):

        if (
            target_table not in self.base_hierarchy
            and target_table in self._available_table_configs
        ):
            self._generate_base_hierarchy(target_table)

        children = self.base_hierarchy[target_table]

        target_hierarchy_dict = {}
        target_hierarchy_dict.setdefault(target_table, [])

        for child in children:
            if child in self._available_table_configs:
                child_dict = self._generate_single_target_table_hierarchy(child)
                target_hierarchy_dict[target_table].append(child_dict)
            else:
                target_hierarchy_dict[target_table].append(child)
        return target_hierarchy_dict

    @staticmethod
    def _extract_available_configs(config_folder_path):
        available_tables = []
        for file in sorted(os.listdir(config_folder_path)):
            target_table, _ = os.path.splitext(file)
            available_tables.append(target_table)
        return available_tables

    @staticmethod
    def _get_from_clauses(flat_config_dict):

        return [value for key, value in flat_config_dict.items() if "from_S" in key]

    @staticmethod
    def _parse_single_clause(clause):

        parsed_statement = sqlparse.parse(clause)[0]
        identifiers = list(
            sqlparse.sql.IdentifierList(parsed_statement.tokens).get_identifiers()
        )
        clean_identifiers = set(
            iden
            for iden in identifiers
            if iden.__class__ == sqlparse.sql.Identifier or len(identifiers) == 1
        )

        return clean_identifiers

    @staticmethod
    def _sanitize_identifiers(clean_identifiers):
        """
        This is a helper function as sometimes sqlparse returns a comparison object
        (f.e. for "...AND g.some_datetime BETWEEN d.start_date AND d.end_date..." clause
        'g.some_datetime' would be returned as Identifier evne though it's not in this case.)
        so this function collects all aliases from the other Identifier objects and checks if any of
        them match the table name. So for the above example we would expect that there exists a part
        of the clause similar to "... some_table g ..." which would eliminate 'g.some_datetime' as
        true table.

        Args:
            clean_identifiers (List(sqlparse.sql.Identifier)):
            A list containing sqlparse.sql.Identifier objects

        Returns:
            List(str): A list of table names as strings.
        """

        all_aliases = [
            iden.get_name() for iden in clean_identifiers if iden.has_alias()
        ]

        sanitized_identifier_names = set(
            iden.normalized
            for iden in clean_identifiers
            if iden.normalized.split(".")[0] not in all_aliases
        )

        return sanitized_identifier_names
