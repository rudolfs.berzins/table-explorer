import argparse
import os
import sys

from pathlib import Path

from table_explorer.hierarchy_generator import HierarchyGenerator
from table_explorer.ascii_printer import AsciiPrinter
from table_explorer.utils.helpers import grouped, extract_key


def args_parser():

    parser = argparse.ArgumentParser(
        description="Given a folder containing JSON config files, generate table hierarchies for either all config files in folder or a specific target table."
    )
    parser.add_argument(
        "-c",
        "--config_folder",
        help="Path to folder containing JSON configuration files",
        required=True,
    )
    parser.add_argument(
        "-a",
        "--all",
        action="store_true",
        help="Display hierarchies for all configuration files in the given folder. This option is overridden if '-t' option is present!",
    )
    parser.add_argument(
        "-t",
        "--target_tables",
        help="Denotes which table (or tables) to target. The tables configuration file MUST be present in the configuration folder (given by '-c' parameter)",
        nargs="*",
        default=[None],
    )
    parser.add_argument(
        "--show_tables",
        action="store_true",
        help="Display all available tables from the config folder",
    )
    parser.add_argument(
        "--save_as_markdown",
        action="store_true",
        help="If set the output of the individual hierarchies will be saved into a folder given by '--output_folder_path' argument. Default is current working directory.",
    )
    parser.add_argument(
        "--output_folder_path",
        help="If '--save_as_markdown' is present then stores the path to the folder where the individual hierarchy output files need to be saved. Default is current working directory.",
        default=Path.cwd(),
    )

    return parser


def show_tables(config_folder, available_tables, output=sys.stdout):

    print(f"All available tables in {config_folder}\n")
    for x, y in grouped(available_tables, 2):
        print(f"{x:40}  {y if y is not None else ''}", file=output)


def explore_tables(args=None):

    if args is None:
        args = args_parser().parse_args()

    HG = HierarchyGenerator(args.config_folder, all_indicator=args.all)

    if args.show_tables:
        show_tables(args.config_folder, HG._available_table_configs)
        sys.exit(0)

    hierarchies = HG.generate_hierarchies(args.target_tables)
    AP = AsciiPrinter()

    for hierarchy in hierarchies:
        output_file = sys.stdout
        target = extract_key(hierarchy)
        if args.save_as_markdown:
            Path(args.output_folder_path).mkdir(parents=True, exist_ok=True)
            output_file = os.path.join(args.output_folder_path, f"{target}.md")
        AP.print_table_struct(hierarchy, output_file)


if __name__ == "__main__":
    explore_tables()
