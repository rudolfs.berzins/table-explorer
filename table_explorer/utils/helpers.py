from collections.abc import MutableMapping
from itertools import zip_longest
import json


def read_json_file(json_file_path):

    return json.load(open(json_file_path))


def flatten(d, parent_key="", sep="_"):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        v = v if isinstance(v, list) else [v]
        for elem in v:
            if isinstance(elem, MutableMapping):
                items.extend(flatten(elem, new_key, sep=sep).items())
            else:
                items.append((new_key, elem))
    return dict(items)


def grouped(iterable, n):
    "s -> (s0,s1,s2,...sn-1), (sn,sn+1,sn+2,...s2n-1), (s2n,s2n+1,s2n+2,...s3n-1), ..."
    return zip_longest(*[iter(iterable)] * n)


def extract_key(path_dict):
    return list(path_dict.keys())[0]
