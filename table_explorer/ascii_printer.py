from collections.abc import MutableMapping
import sys

from table_explorer.utils.helpers import extract_key

PIPE = "|"
PIPE_PLUS = "|+"
BASE_SEP = "    "
PIPE_SEP = "|   "


class AsciiPrinter:
    """
    Inspired by - tinyurl.com/bdhkraxv
    """

    def __init__(self):
        self._table_struct = []

    def build_table_struct(self, path_dict):
        self._table_struct = []
        root = extract_key(path_dict)
        self._table_struct_head(root)
        self._table_struct_body(path_dict[root])
        return self._table_struct

    def print_table_struct(self, path_dict, output_file):

        self._table_struct = [
            line for line in self.build_table_struct(path_dict) if line
        ]

        self._table_struct.append("")

        if output_file != sys.stdout:
            self._table_struct.insert(0, "```")
            self._table_struct.append("```")
            output_file = open(output_file, mode="w", encoding="UTF-8")

        for line in self._table_struct:
            print(line, file=output_file)

        if output_file != sys.stdout:
            output_file.close()

    def _table_struct_head(self, root):
        self._table_struct.append(f"{root}")
        self._table_struct.append(f"{BASE_SEP}{PIPE}")

    def _table_struct_body(self, target_list, prefix=f"{BASE_SEP}"):

        sorted_list = sorted(target_list, key=lambda x: len(str(x)))
        target_count = len(target_list)
        for target_idx, target in enumerate(sorted_list):
            if isinstance(target, str):
                self._add_singleton(target_idx, target_count, target, prefix)
            elif isinstance(target, MutableMapping):
                self._add_map(target_idx, target_count, target, prefix)

    def _add_singleton(self, target_index, target_count, target, prefix):
        self._table_struct.append(f"{prefix}{PIPE_PLUS}{target}")
        if target_index != target_count - 1:
            self._table_struct.append(f"{prefix}{PIPE}")

    def _add_map(self, target_index, target_count, target, prefix):
        key = extract_key(target)
        self._table_struct.append(f"{prefix}{PIPE_PLUS}{key}")
        if target_index != target_count - 1:
            prefix += PIPE_SEP
        else:
            prefix += BASE_SEP
        self._table_struct.append(f"{prefix}{PIPE}")
        self._table_struct_body(target_list=target[key], prefix=prefix)
        self._table_struct.append(prefix.rstrip())
