import os

import pytest

from table_explorer.ascii_printer import AsciiPrinter
from table_explorer.hierarchy_generator import HierarchyGenerator

### ASCII Printer Fixtures


@pytest.fixture()
def initialized_ascii_printer():
    return AsciiPrinter()


@pytest.fixture(scope="session")
def strings_only_map():
    return {"test_key": ["test_string_1", "test_string_2"]}


@pytest.fixture(scope="session")
def maps_w_strings_map():
    return {"outer_test_key": ["test_string_1", {"inner_test_key": ["test_string_2"]}]}


@pytest.fixture(scope="session")
def map_w_inner_maps_map():
    return {
        "outer_test_key": [
            "test_string_1",
            "test_string_2",
            {
                "middle_test_key_1": [
                    "test_string_3",
                    {"inner_test_key": ["test_string_4"]},
                ]
            },
            {"middle_test_key_2": ["test_string_5"]},
        ]
    }


@pytest.fixture(scope="session")
def test_target_list(map_w_inner_maps_map):
    return [map_w_inner_maps_map]


@pytest.fixture(scope="session")
def test_path_dict(test_target_list):
    return {"test": test_target_list}


### Hierarchy Extractor Fixtures
@pytest.fixture()
def initialized_hierarchy_generator(test_config_folder_path):
    return HierarchyGenerator(test_config_folder_path)


@pytest.fixture(scope="session")
def test_config_folder_path():
    return os.path.abspath("tests/test_data")


@pytest.fixture(scope="session")
def test_sql_clause():
    return "test_database_1.test_table_1 t1 INNER JOIN test_database_2.test_table_2 t2 ON t1.com_1 = t2.com_2 AND t1.individual_identifier BETWEEN t2.some_start AND t2.some_end"
