import tempfile

import pytest

from table_explorer.table_explorer import args_parser, show_tables, explore_tables


@pytest.mark.unit
def test_show_tables_correctly_prints_output(test_config_folder_path):

    input_available_tables = ["a", "b"]

    expected_output = ["a                                         b\n"]

    with tempfile.NamedTemporaryFile(encoding="utf-8", mode="w") as tmp:
        show_tables(test_config_folder_path, input_available_tables, output=tmp)

        tmp.seek(0)

        test_output = [line for line in open(tmp.name, mode="r")]

    assert test_output == expected_output


@pytest.mark.validation
def test_explore_tables_works_correctly(test_config_folder_path, capsys):

    args_string = f"-c {test_config_folder_path} -t test.config"

    args = args_parser().parse_args(args_string.split())

    explore_tables(args)

    # Assert that hierarchy was printed on screen
    hierarcy_out, _ = capsys.readouterr()

    assert hierarcy_out
