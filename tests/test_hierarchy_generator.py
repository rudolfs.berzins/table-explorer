import pytest

from table_explorer.utils.exceptions import MissingTargetTableName


@pytest.mark.unit
def test__extract_available_configs_correctly_gets_all_available_configs(
    initialized_hierarchy_generator,
):

    expected_tables = ["test.config"]

    test_tables = initialized_hierarchy_generator._extract_available_configs(
        "tests/test_data/"
    )

    assert test_tables == expected_tables


@pytest.mark.unit
def test__get_from_clauses_correctly_extracts_value_from_flat_config_dict(
    initialized_hierarchy_generator,
):

    input_flat_config_dict = {"test_from_S": "good", "test_other_A": "bad"}

    expected_output = ["good"]

    test_output = initialized_hierarchy_generator._get_from_clauses(
        input_flat_config_dict
    )

    assert test_output == expected_output


@pytest.mark.skip(
    reason="TODO: find a way to create intermediate sqlparse.sql.Identifier objects"
)
def test__parse_single_clause_correctly_extracts_clean_identifiers(
    initialized_hierarchy_generator, test_sql_clause
):
    """Read skip reason"""
    pass


@pytest.mark.unit
def test__sanitize_identifiers_correctly_returns_only_table_names(
    initialized_hierarchy_generator, test_sql_clause
):

    input_clean_identifiers = initialized_hierarchy_generator._parse_single_clause(
        test_sql_clause
    )

    expected_output = set(
        ["test_database_1.test_table_1", "test_database_2.test_table_2"]
    )

    test_output = initialized_hierarchy_generator._sanitize_identifiers(
        input_clean_identifiers
    )

    assert test_output == expected_output


@pytest.mark.integration
def test__parse_single_config_dict_correctly_parses_a_flat_config_dict(
    initialized_hierarchy_generator, test_sql_clause
):

    input_flat_config_dict = {"test_from_S": test_sql_clause}

    expected_output = set(
        ["test_database_1.test_table_1", "test_database_2.test_table_2"]
    )

    test_output = initialized_hierarchy_generator._parse_single_config_dict(
        input_flat_config_dict
    )

    assert test_output == expected_output


@pytest.mark.integration
def test__generate_base_hierarchy_correctly_adds_entry_to_base_hierarchy(
    initialized_hierarchy_generator,
):

    input_target_table = "test.config"

    expected_output = {"test.config": set(["base.test", "dict.test"])}

    initialized_hierarchy_generator._generate_base_hierarchy(input_target_table)

    test_output = initialized_hierarchy_generator.base_hierarchy

    assert test_output == expected_output


@pytest.mark.integration
def test__generate_single_target_table_hierarchy_correctly_creates_a_target_hierarchy_dict(
    initialized_hierarchy_generator,
):

    input_target_table = "test.config"

    expected_output = {"test.config": ["base.test", "dict.test"]}

    test_output = (
        initialized_hierarchy_generator._generate_single_target_table_hierarchy(
            input_target_table
        )
    )

    assert sorted(test_output["test.config"]) == sorted(expected_output["test.config"])


@pytest.mark.unit
def test_generate_hierarchies_correctly_raises_custom_exception_when_target_table_is_missing(
    initialized_hierarchy_generator,
):

    faulty_target_table = "missing.table"

    with pytest.raises(MissingTargetTableName):
        initialized_hierarchy_generator.generate_hierarchies(faulty_target_table)


@pytest.mark.validation
def test_generate_hierarchies_correctly_returns_list_of_hierarchies(
    initialized_hierarchy_generator,
):

    input_target_tables = ["test.config"]
    faulty_target_tables = ["test.config", "missing.config"]

    expected_output = [{"test.config": ["base.test", "dict.test"]}]

    # HierarchyGenerator correctly generates hierarchies when all requested tables are present in config folder
    test_output = initialized_hierarchy_generator.generate_hierarchies(
        input_target_tables
    )

    assert sorted(test_output[0]["test.config"]) == sorted(
        expected_output[0]["test.config"]
    )

    # HierarchyGenerator raises MissingTargetTableName if one or more of the target tables is missing a config

    with pytest.raises(MissingTargetTableName):
        initialized_hierarchy_generator.generate_hierarchies(faulty_target_tables)


@pytest.mark.validation
def test_generate_hierarchies_correctly_handles__all_indicator_flag(
    initialized_hierarchy_generator,
):

    input_target_table = ["test.config"]
    faulty_target_table = ["missing.table"]

    expected_output = [{"test.config": ["base.test", "dict.test"]}]

    initialized_hierarchy_generator._all_indicator = True

    # HierarchyGenerator returns all hierarchies if no target_table given, but self._all_indicator is True

    test_output_all_indicator_true = (
        initialized_hierarchy_generator.generate_hierarchies()
    )

    assert sorted(test_output_all_indicator_true[0]["test.config"]) == sorted(
        expected_output[0]["test.config"]
    )

    # HierarchyGenerator returns correct hierarcy if both target_table given and self._all_indicator is True

    test_output_both_present = initialized_hierarchy_generator.generate_hierarchies(
        input_target_table
    )

    assert sorted(test_output_both_present[0]["test.config"]) == sorted(
        expected_output[0]["test.config"]
    )

    # HierarchyGenerator values target_table over self._all_indicator
    # so if both are present, but target_table name is missing it will raise MissingTargetTableName

    with pytest.raises(MissingTargetTableName):
        initialized_hierarchy_generator.generate_hierarchies(faulty_target_table)
