import json
import tempfile

import pytest

from table_explorer.utils.helpers import read_json_file, flatten, grouped, extract_key


@pytest.mark.unit
def test_read_json_file_takes_file_path_and_returns_a_dictionary(
    test_config_folder_path,
):
    expected_dict = {"test": "dict"}

    with tempfile.NamedTemporaryFile() as tmp:
        tmp.write(json.dumps(expected_dict).encode("utf-8"))
        tmp.seek(0)

        file_path = tmp.name

        test_dict = read_json_file(file_path)

    assert test_dict == expected_dict


@pytest.mark.unit
def test_flatten_flattens_a_nested_dictionary():

    input_dict = {"A": {"B": 1, "C": 2}}

    expected_dict = {"A_B": 1, "A_C": 2}

    test_dict = flatten(input_dict)

    assert test_dict == expected_dict


@pytest.mark.unit
def test_flatten_correctly_handles_inner_lists():

    input_dict = {"A": [{"B": 1}, {"C": 2}], "D": {"E": 3, "F": 4, "G": [{"H": 5}]}}

    expected_dict = {"A_B": 1, "A_C": 2, "D_E": 3, "D_F": 4, "D_G_H": 5}

    test_dict = flatten(input_dict)

    assert test_dict == expected_dict


@pytest.mark.unit
@pytest.mark.parametrize(
    "input_n, expected_output",
    [
        (1, [tuple([i]) for i in [1, 2, 3, 4, 5, 6]]),
        (2, [(1, 2), (3, 4), (5, 6)]),
        (3, [(1, 2, 3), (4, 5, 6)]),
        (4, [(1, 2, 3, 4), (5, 6, None, None)]),
    ],
)
def test_grouped_correctly_returns_tuples_of_length_n(input_n, expected_output):

    input_list = [1, 2, 3, 4, 5, 6]

    test_output = [i for i in grouped(input_list, input_n)]

    assert test_output == expected_output


@pytest.mark.unit
def test_extract_key_correctly_takes_first_key():

    input_dict = {"test": "dict", "test_2": "dict_2"}

    expected_output = "test"

    test_output = extract_key(input_dict)

    assert test_output == expected_output
