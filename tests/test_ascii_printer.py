import pytest


@pytest.mark.unit
def test__add_singleton_correctly_adds_entry_to_the_table_struct(
    initialized_ascii_printer,
):

    expected_lines = ["    |+test", "    |"]
    expected_line_count = len(expected_lines)

    target_index = 1
    target_count = 4
    target = "test"
    prefix = "    "

    initialized_ascii_printer._add_singleton(target_index, target_count, target, prefix)

    assert len(initialized_ascii_printer._table_struct) == expected_line_count
    assert initialized_ascii_printer._table_struct == expected_lines


@pytest.mark.unit
def test__add_singleton_correctly_adds_only_one_line_at_end(initialized_ascii_printer):

    expected_lines = ["    |+test"]
    expected_line_count = len(expected_lines)

    target_index = 3
    target_count = 4
    target = "test"
    prefix = "    "

    initialized_ascii_printer._add_singleton(target_index, target_count, target, prefix)

    assert len(initialized_ascii_printer._table_struct) == expected_line_count
    assert initialized_ascii_printer._table_struct == expected_lines


@pytest.mark.unit
def test__add_map_correctly_handles_strings_only_map(
    initialized_ascii_printer, strings_only_map
):

    expected_lines = [
        "    |+test_key",
        "        |",
        "        |+test_string_1",
        "        |",
        "        |+test_string_2",
        "",
    ]
    expected_line_count = len(expected_lines)

    target_index = 3
    target_count = 4
    target = strings_only_map
    prefix = "    "

    initialized_ascii_printer._add_map(target_index, target_count, target, prefix)

    assert initialized_ascii_printer._table_struct == expected_lines
    assert len(initialized_ascii_printer._table_struct) == expected_line_count


@pytest.mark.unit
def test__add_map_correctly_handles_maps_w_strings_map(
    initialized_ascii_printer, maps_w_strings_map
):

    expected_lines = [
        "    |+outer_test_key",
        "        |",
        "        |+test_string_1",
        "        |",
        "        |+inner_test_key",
        "            |",
        "            |+test_string_2",
        "",
        "",
    ]
    expected_line_count = len(expected_lines)

    target_index = 3
    target_count = 4
    target = maps_w_strings_map
    prefix = "    "

    initialized_ascii_printer._add_map(target_index, target_count, target, prefix)

    assert initialized_ascii_printer._table_struct == expected_lines
    assert len(initialized_ascii_printer._table_struct) == expected_line_count


@pytest.mark.unit
def test__add_map_correctly_handles_map_w_inner_maps_map(
    initialized_ascii_printer, map_w_inner_maps_map
):

    expected_lines = [
        "    |+outer_test_key",
        "        |",
        "        |+test_string_1",
        "        |",
        "        |+test_string_2",
        "        |",
        "        |+middle_test_key_2",
        "        |   |",
        "        |   |+test_string_5",
        "        |",
        "        |+middle_test_key_1",
        "            |",
        "            |+test_string_3",
        "            |",
        "            |+inner_test_key",
        "                |",
        "                |+test_string_4",
        "",
        "",
        "",
    ]

    expected_line_count = len(expected_lines)

    target_index = 3
    target_count = 4
    target = map_w_inner_maps_map
    prefix = "    "

    initialized_ascii_printer._add_map(target_index, target_count, target, prefix)

    assert initialized_ascii_printer._table_struct == expected_lines
    assert len(initialized_ascii_printer._table_struct) == expected_line_count


@pytest.mark.unit
def test__table_struct_body_correctly_handles_complex_target_list(
    initialized_ascii_printer, test_target_list
):

    expected_lines = [
        "    |+outer_test_key",
        "        |",
        "        |+test_string_1",
        "        |",
        "        |+test_string_2",
        "        |",
        "        |+middle_test_key_2",
        "        |   |",
        "        |   |+test_string_5",
        "        |",
        "        |+middle_test_key_1",
        "            |",
        "            |+test_string_3",
        "            |",
        "            |+inner_test_key",
        "                |",
        "                |+test_string_4",
        "",
        "",
        "",
    ]

    expected_line_count = len(expected_lines)

    target_list = test_target_list

    initialized_ascii_printer._table_struct_body(target_list)

    assert initialized_ascii_printer._table_struct == expected_lines
    assert len(initialized_ascii_printer._table_struct) == expected_line_count


@pytest.mark.unit
def test__table_struct_head_correctly_adds_first_lines(initialized_ascii_printer):

    expected_lines = ["test", "    |"]
    expected_line_count = len(expected_lines)

    test_root = "test"

    initialized_ascii_printer._table_struct_head(test_root)

    assert initialized_ascii_printer._table_struct == expected_lines
    assert len(initialized_ascii_printer._table_struct) == expected_line_count


@pytest.mark.validation
def test_build_table_struct_correctly_creates_table_struct_lines(
    initialized_ascii_printer, test_path_dict
):

    expected_lines = [
        "test",
        "    |",
        "    |+outer_test_key",
        "        |",
        "        |+test_string_1",
        "        |",
        "        |+test_string_2",
        "        |",
        "        |+middle_test_key_2",
        "        |   |",
        "        |   |+test_string_5",
        "        |",
        "        |+middle_test_key_1",
        "            |",
        "            |+test_string_3",
        "            |",
        "            |+inner_test_key",
        "                |",
        "                |+test_string_4",
        "",
        "",
        "",
    ]
    expected_line_count = len(expected_lines)

    test_table_struct = initialized_ascii_printer.build_table_struct(test_path_dict)

    assert test_table_struct == expected_lines
    assert expected_line_count
