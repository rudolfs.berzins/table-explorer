#!/bin/bash

# Create a virtual environment and activate it
python3 -m venv table_explorer_venv
source table_explorer_venv/bin/activate

# Install poetry
pip install poetry

# Install package
poetry install